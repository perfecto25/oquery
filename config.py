# Maestro Configuration


SECRET_KEY = 'beijox9samba'
WTF_CSRF_ENABLED = True
PORT = 5700
APP_DIR = '/home/mrx/dev/oquery'
LOG_DIR = APP_DIR + '/log'
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + APP_DIR + '/app/blocks/auth/users.db'
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Oracle connection
ORACLE_LIBS = '/usr/lib/oracle/12.2/client64/lib/'
ORACLE_URL = ''
ORACLE_USER = 'orclUser'
ORACLE_PW = 'mypasswd'
ORACLE_DB = 'myDB'
LD_LIBRARY_PATH = ORACLE_LIBS

