# OQuery
## Oracle Query portal based on python Flask web framework

## Purpose
creates a user-friendly web interface for Oracle queries

## Technical
- OQuery app runs on a python Flask web framework
- Flask app is managed by Gunicorn, a Python WSGI web server that can serve worker threads
    - Gunicorn is started via a Startup script (install/oquery.service.init), this script is placed in /etc/init.d and is added to chkconfig by the install_oquery.sh script

- The site is served to the public using NGINX web server, that listens for a gunicorn process running on port 5700
    - Nginx configuration is located in install/nginx folder

- OQuery requires Oracle libraries to be present, see the Oracle section in install_oquery.sh script for specific steps. The file 'env.sh' provides the startup script with environment variables for Oracle lib location

## Installation
1. install python-pip, nginx
1. download Oracle instantclient x64, extract all .so files, place them into install/oracle_libs folder
1. run install/install_oquery.sh
1. add your Oracle credentials to 'secure' file

    ``` source secure ```
1. start service 

    ``` service oquery restart ```
1. start nginx

    ``` service nginx start ```


## User Logins
User access is controlled by Flask-Login module and uses SQLite DB to store user accounts. All user passwords are hashed with salt to provide a more secure access.

To create a new Database

``` 
cd APP_DIR/app/blocks/auth
python create_db.py
```
a new database called "users.db" will be created in APP_DIR/app/blocks/auth

To add users to the Database,

edit the file  
``` APP_DIR/app/blocks/auth/users.secure.json ```

add your users, email and password to the JSON

Now update the database by running,

``` 
python create_users.py
```

This will add the users in the users.secure.json. After database is updated, you can delete the JSON file.

To secure routes for user login, add the ```@login_required``` decorator to a route




## Troubleshooting
oquery.sock permissions

``` srwxrw-rw- 1 ec2-user ec2-user    0 May 17 20:21 oquery.sock ```

## Release Notes

### 0.02
- removed secure file, all config values come from config.py
- added Logging to application
- added error handling (404, exceptions)
- added Flask-Login system, not using Nginx for user login

### 0.01
- initial release
