#!/usr/bin/env python
# coding=utf-8

# Various helper functions

import os
import json
import logging
from app import app


# Logging Globals
LOG_FORMAT = "%(asctime)s [%(levelname)s] %(message)s"

# Setup Logging
def init_log(log_name):
    ''' configures rotating log file for the Project '''
    from logging.handlers import RotatingFileHandler
    
    log_dir = app.config['LOG_DIR']
    log_file = log_dir + '/' + log_name + '.log'

    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    global log
    log = logging.getLogger(log_name)
    handler = RotatingFileHandler(log_file, maxBytes=5000000, backupCount=3)
    formatter = logging.Formatter(LOG_FORMAT)
    handler.setFormatter(formatter)
    log.addHandler(handler)
    log.setLevel(logging.DEBUG)
    return log

def dictator(data, path=None, default=None, checknone=False):
    '''
    The Dictator function takes a dictionary or JSON data and returns value for a specific key.
    If dictator doesnt find a value for a key, or the data is missing the key, the return value is
    either None or whatever fallback value you provide as default="My Default Fallback value".
    Dictator is polite with Exception errors commonly encountered when parsing large Dictionaries/JSONs
    Usage:
    get a value from a Dictionary key
    > dictator(data, "employees[5].firstName")
    with custom value on lookup failure,
    > dictator(data, "employees[5].firstName", default="No employee found")
    pass a parameter
    > dictator(data, "company.name.{}".format(my_company))
    lookup a 3rd element of List Json, on second key, lookup index=5
    > dictator(data, "3.first.second[5]")
    lookup a nested list of lists
    > dictator(data, "0.first[1].2.second.third[0].2"
    check if return value is None, if it is, raise a Value error
    > dictator(data, "some.key.value", checknone=True)
    > error Value Error
    '''

    import json

    if path is None or path == '':
        return json.dumps(data)

    value = None
    keys = path.split(".")

    # reset path
    path = None

    # if 1st key is a list index
    if all(char.isdigit() for char in keys[0]):
        path = '['+keys[0]+']'

        # remove 1st key from key list
        keys.pop(0)

    # build proper path
    for key in keys:
        # check if key is a list
        if key.endswith(']'):
            temp = key.split('[')
            key = ''.join(temp[0])
            index = int(temp[1].strip(']'))
            if path is None:
                path = "['"+key+"']"+"["+str(index)+"]"
            else:
                path = path+"['"+key+"']"+"["+str(index)+"]"
        else:
            if path is None:
                path = "['"+key+"']"

            else:
                # check if key is an index
                if key.isdigit() is True:
                    path = path + "["+key+"]"
                else:
                    path = path + "['"+key+"']"
    lookup = 'data'+path
    try:
        value = eval(lookup)
        if value is None:
            value = default
    except (KeyError, ValueError, IndexError, TypeError) as err:
        value = default
    finally:
        if checknone:
            if not value:
                log.error('missing value for %s' % path)
                raise ValueError('missing or null value for %s' % path)
    return value