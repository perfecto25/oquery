# Email Prod Change Notice

from flask import render_template, redirect, url_for, flash, request, session, abort
from app import app, log


# ERROR HANDLERS
@app.errorhandler(404)
def page_not_found(error):
    log.error('Page not found: %s', (request.path))
    return render_template('errors/404.html', title='404 Error', msg=request.path)

@app.errorhandler(500)
def internal_server_error(error):
    log.error('Server Error: %s' % error)
    return render_template('errors/500.html', title='500 Error', msg=error)

@app.errorhandler(Exception)
def unhandled_exception(e):
    log.error('Unhandled Exception: %s' % str(e))
    return render_template('errors/error.html', title='Exception', msg=str(e)), 500

