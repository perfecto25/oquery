# Email Prod Change Notice

from flask import render_template, redirect, url_for, flash, request, session, abort
from app import app, log
from app.blocks.oracle.forms import LookupFormInit, LookupFormResult
from app.blocks.oracle.oracle import run_attomid_query
from app.blocks.oracle.functions import make_attomid_dict
from flask_login import login_required, logout_user, current_user

import sys

# ROUTING
@app.route('/')
def index():
    ''' index page '''
    return render_template('pages/index.html', title='Roc Portal')

@app.route('/lookup', methods=['GET', 'POST'])
@login_required
def lookup_init():
    ''' DB lookup '''
    
    form = LookupFormInit()

    # on Search submit, redirect to 'do_db_lookup'
    if form.validate_on_submit():
        lookup_key = form.lookup.data
        return redirect(url_for('do_db_lookup', 
                                lookup_key=lookup_key))
    
    # generate Init form to submit query 
    return render_template('forms/lookup_result.html', 
                          form=form, 
                          title='get DB value'
                          )


@app.route('/lookup/<lookup_key>', methods=['GET', 'POST'])
@login_required
def do_db_lookup(lookup_key):
    ''' show query result '''

    form = LookupFormResult()

    result = run_attomid_query(lookup_key)

    # generate Dict
    if not result == 'NA':
        result_dict = make_attomid_dict(result)
    else:
        result_dict = 'No results found'


    return render_template('forms/lookup_result.html', 
                            action='attomid',
                            result=result_dict,
                            title='AttomID Query Result',  
                            lookup_key=lookup_key)


@app.route('/error/<lookup_key>', methods=['GET', 'POST'])
@login_required
def error(lookup_key):
    ''' error processing '''
    flash('SQL query error', 'danger')
    return render_template('layouts/main.html', 
                            title='Query - Error', 
                            lookup_key=lookup_key)