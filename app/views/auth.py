
from flask import render_template, redirect, url_for, flash, request, session, abort
from app.models import User
from flask_sqlalchemy import SQLAlchemy
from flask_login import login_user, login_required, logout_user, UserMixin, login_manager, current_user
from app.forms import LoginForm
from app import app, log
from werkzeug.urls import url_parse


@app.route('/login', methods=['GET', 'POST'])
def login():
    log.info('debug1')
    form = LoginForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            log.info('debug2')
            user = User.query.filter_by(email=form.email.data).first()
            log.info(user)
            if user is None or not user.check_password(form.password.data):
                flash('Invalid Email or Password')
                return redirect(url_for('login'))
            log.info(user.check_password(form.password.data))
            login_user(user, remember=form.remember_me.data)
            next_page = request.args.get('next')
            if not next_page or url_parse(next_page).netloc != '':
                next_page = url_for('index')
            return redirect(next_page)
        else:
            flash('check for proper Email format, must be in form of username@company.com')
            return render_template('forms/login.html', title='Sign In', form=form)\

    if request.method == 'GET':
        return render_template('forms/login.html', title='Sign In', form=form)
    
@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))