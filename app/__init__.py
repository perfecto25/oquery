from flask import Flask, request, url_for, render_template, flash, redirect
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object('config')
Bootstrap(app)

# Authentication
login = LoginManager(app)
login.session_protection = 'strong'
login.login_view = 'login'
login.init_app(app)

db = SQLAlchemy(app)

# start Logging
from app.functions import init_log
log = init_log('oquery')

from views import main, errors, auth
from app import models


