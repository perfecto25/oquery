from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, TextField, BooleanField
from wtforms.validators import DataRequired, EqualTo, Length, Email
from wtforms.fields.html5 import DateField

class LoginForm(FlaskForm):
    #username = StringField('username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField("Log In")