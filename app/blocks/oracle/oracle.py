#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import cx_Oracle
from app import app
from app.functions import dictator
from app import log

def run_attomid_query(lookup_key):
    
    # get Oracle creds from config
    ORACLE_USER = dictator(app.config, 'ORACLE_USER', checknone=True)
    ORACLE_PW = dictator(app.config, 'ORACLE_PW', checknone=True)
    ORACLE_URL = dictator(app.config, 'ORACLE_URL', checknone=True)
    ORACLE_DB = dictator(app.config, 'ORACLE_DB', checknone=True)
    
    try:
        connection = '%s/%s@%s/%s' % (ORACLE_USER, ORACLE_PW, ORACLE_URL, ORACLE_DB)   
        conn = cx_Oracle.connect(connection)

    except cx_Oracle.DatabaseError, e:
        log.error('failed to connect to DB Host: %s') % ORACLE_URL
        log.exception(e)
        sys.exit(1)

# #print conn.version()
    cursor = conn.cursor()
    try:
        cursor.execute('select * from ROCCAP.ATTOM_ACTIVITY_V where attomid = %s' % lookup_key)
    except Exception as e:
        log.error('failed running query: %s') % str(e)
    
    # create result List
    result = []

    for row in cursor:
        result.append(row)    
   # row = cursor.fetchone()

    
    if not result:
        result = 'NA'
    
    return result