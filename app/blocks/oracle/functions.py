#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
from app import app


# OQuery functions

def make_attomid_dict(result):
    ''' creates a Dictionary based on attomid query result '''
    result_dict = {}

    total = len(result)
    current = 0
    while current < total:
        row = result[current]
        result_dict[current] = {}
        result_dict[current]['attomid'] = row[0]
        result_dict[current]['action date'] = row[1]
        result_dict[current]['purchase date'] = row[2]
        result_dict[current]['sell date'] = row[3]
        result_dict[current]['recording date'] = row[4]
        result_dict[current]['description'] = row[5]
        result_dict[current]['seller'] = row[6]
        result_dict[current]['buyer'] = row[7]
        result_dict[current]['lender'] = row[8]

        if row[9]:
            result_dict[current]['loan amount'] = "{:,}".format(row[9])
        else:
            result_dict[current]['loan amount'] = row[9]
        if row[10]:
            result_dict[current]['sell price'] = "{:,}".format(row[10])
        else:
            result_dict[current]['sell price'] = row[10]

        result_dict[current]['interest rate'] = row[11]
        current += 1

    return result_dict