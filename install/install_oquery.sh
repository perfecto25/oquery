#!/bin/bash
# OQuery Install and Config (initd systems only!)

# MANUAL STEPS REQUIRED:
# 1. install nginx
# 2. install python-pip


user=ec2-user
app_dir=/home/$user/oquery

# Oracle client libs
sudo mkdir -p /usr/lib/oracle/12.2/client64/lib/
sudo cp oracle_libs/* /usr/lib/oracle/12.2/client64/lib/



# VirtualEnv
cd $app_dir && virtualenv venv
source $app_dir/venv/bin/activate

# Python deps
cd $app_dir/install
sudo pip install -r requirements.txt

# startup script
sudo mkdir -p /var/log/gunicorn
sudo chown $user:$user /var/log/gunicorn
sudo cp oquery.service.systemd  /etc/systemd/system/oquery.service
sudo systemctl enable oquery.service
sudo systemctl daemon-reload

#sudo chmod +x /etc/init.d/oquery
#sudo chkconfig --add oquery
#sudo chkconfig oquery on
#sudo service oquery restart

# NGINX
sudo cp oquery.nginx.conf /etc/nginx/sites-available/oquery
sudo ln -s /etc/nginx/sites-available/oquery /etc/nginx/sites-enabled
sudo service nginx restart